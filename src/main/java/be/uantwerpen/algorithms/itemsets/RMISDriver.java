/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.algorithms.itemsets;

import static be.uantwerpen.util.ScoringFunctions.equalScoringFunction;
import static be.uantwerpen.util.ScoringFunctions.ruleScoringFunction;
import static be.uantwerpen.util.ScoringFunctions.supportScoringFunction;
import static be.uantwerpen.util.SetReporters.cmdReporter;
import static be.uantwerpen.util.SetReporters.fileReporter;
import static be.uantwerpen.util.WeightingSchemes.adaptiveWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.additiveWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.multiplicativeWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.noWeightScheme;
import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import be.uantwerpen.util.ScoringFunction;
import be.uantwerpen.util.SetReporter;
import be.uantwerpen.util.WeightingScheme;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTransactionFileFactory;
import de.unibonn.realkd.patterns.rules.AssociationRuleMeasurementProcedures;
import de.unibonn.realkd.patterns.rules.DefaultConfidenceMeasurementProcedure;

/**
 * Driver class for sampling maximal itemsets from transactional databases
 * 
 * @author Sandy Moens
 * @version 3.0.0
 * @since 2.0.0
 */
public class RMISDriver {

	/**
	 * Exception when specifying illegal options
	 */
	private static class InvalidOption extends Exception {

		private static final long serialVersionUID = -2415730924422766253L;

		private final String option;

		public InvalidOption(String option) {
			this.option = option;
		}

		@Override
		public String getMessage() {
			return "Invalid option: " + this.option;
		}
	}

	private static final String Help = "help";

	private static final String Quality = "quality";
	private static final String Approximation = "approximation";
	private static final String Weighting = "weighting";
	private static final String Output = "output";

	private static final String nl = "\n";
	private static final String jump = "\t";

	/**
	 * Main driver function
	 * 
	 * @param args
	 *            list of arguments for sampling
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			printHelpMenu();
			return;
		}

		if (Help.startsWith(args[0])) {
			if (args.length == 1) {
				printHelpMenu();
			} else if (Quality.startsWith(args[1])) {
				printQualityMeasures();
			} else if (Approximation.startsWith(args[1]) || args[1].equals("p")) {
				printApproximationMeasures();
			} else if (Weighting.startsWith(args[1])) {
				printWeightingSchemes();
			} else {
				printHelpMenu();
			}
			return;
		} else if (args.length < 3) {
			printHelpMenu();
			return;
		}

		Map<String, String> options = newHashMap();
		try {
			parseOptions(Arrays.copyOf(args, args.length - 3), options);
		} catch (InvalidOption e) {
			System.out.println(e.getMessage() + nl);
			printHelpMenu();
			return;
		}

		String fileName = args[args.length - 3];
		Integer minSup = Integer.parseInt(args[args.length - 2]);
		Integer samples = Integer.parseInt(args[args.length - 1]);

		System.out.print("Reading dataset...");
		PropositionalContext context = PropositionalLogicFromTransactionFileFactory.build(fileName, " ");
		System.out.println("Done!");

		SetReporter setReporter = setReporter(options.get(Output));
		ScoringFunction q = qualityMeasure(options.get(Quality), context);
		ScoringFunction p = approximationMeasure(options.get(Approximation), context);
		WeightingScheme w = weightingScheme(options.get(Weighting));

		printInfo(context, minSup, samples, q, p, w);

		RandomMaximalItemsetSampler rmis = new RandomMaximalItemsetSampler(context);
		rmis.pruningMeasure(q);
		rmis.approximationMeasure(p);
		rmis.weightingScheme(w);
		rmis.setReporter(setReporter);
		long beg = System.currentTimeMillis();
		rmis.run(minSup, samples);
		System.out.println("Time:" + (System.currentTimeMillis() - beg));

		setReporter.close();
	}

	private static void printInfo(PropositionalContext context, Integer minSup, Integer samples, ScoringFunction q,
			ScoringFunction p, WeightingScheme w) {
		StringBuilder builder = new StringBuilder();
		builder.append("Sampling patterns" + nl);
		builder.append("=================" + nl);
		builder.append("Dataset:         " + context.identifier() + nl);
		builder.append("MinSup:          " + minSup + nl);
		builder.append("Samples:         " + samples + nl);
		builder.append("Quality:         " + q.name() + nl);
		builder.append("Approximation:   " + p.name() + nl);
		builder.append("WeightingScheme: " + w.name() + nl);

		System.out.println(builder.toString());
	}

	/**
	 * Prints the help menu
	 */
	private static void printHelpMenu() {
		StringBuilder builder = new StringBuilder();
		builder.append("Please specify:" + jump + "[options] filename minsup sampleSize" + nl + nl);
		builder.append(jump + "List of options:" + nl);
		// @formatter:off
		addOption(builder, "-q", "specifies the quality measure", "support",
				"type \"help q\" for a list of quality measures");
		addOption(builder, "-p", "specifies the approximation measure", "support",
				"type \"help p\" for a list of approximation measures");
		addOption(builder, "-w", "specifies the weighting scheme", "1: none",
				"1: none, 2: additive, 3: multiplicative, 4: adaptive");
		addOption(builder, "-o", "specifies the outputfile", "",
				"if no output is specified, results are written to standard output");
		// @formatter:on
		System.out.println(builder.toString());
	}

	/**
	 * Adds new option to the specified StringBuilder
	 * 
	 * @param builder
	 *            the builder that is expanded
	 * @param option
	 *            option flag ('-F')
	 * @param comment
	 *            comment on how to use the flag
	 * @param defaultValue
	 *            default value for the option
	 * @param extra
	 *            some extra information
	 */
	private static void addOption(StringBuilder builder, String option, String comment, String defaultValue,
			String extra) {
		builder.append(jump + option + jump + comment + nl);
		if (defaultValue.length() != 0) {
			builder.append(jump + jump + "(default: " + defaultValue + ")" + nl);
		}
		if (extra.length() != 0) {
			builder.append(jump + jump + "(" + extra + ")" + nl);
		}
	}

	/**
	 * Prints the different quality measures
	 */
	private static void printQualityMeasures() {
		StringBuilder builder = new StringBuilder();
		builder.append("List of quality measures:" + nl);
		// @formatter:off
		builder.append(jump + "1: Support" + nl);
		builder.append(jump + "2: Average Subset Support" + nl);
		// @formatter:on
		System.out.println(builder.toString());
	}

	/**
	 * Prints the different approximation measures
	 */
	private static void printApproximationMeasures() {
		StringBuilder builder = new StringBuilder();
		builder.append("List of approximation measures:" + nl);
		// @formatter:off
		builder.append("ItemSet Measures:" + nl);
		builder.append(jump + "1: Support" + nl);
		// builder.append(jump + "2: Negative Support" + nl);
		// builder.append(jump + "3: Equal" + nl);
		// builder.append(jump + "4: Average Subset Support" + nl);
		builder.append("Symmetric Rule Measures:" + nl);
		// builder.append(jump + "101: Cosine" + nl);
		// builder.append(jump + "102: Jaccard" + nl);
		// builder.append(jump + "103: Kulcinsky" + nl);
		builder.append(jump + "104: Lift" + nl);
		// builder.append(jump + "105: Odds Ratio" + nl);
		// builder.append(jump + "106: Relative Overlap" + nl);
		// builder.append(jump + "107: Yules Q" + nl);
		// builder.append(jump + "108: Yules Y" + nl);
		builder.append("Asymmetric Rule Measures:" + nl);
		// builder.append(jump + "201: Accuracy" + nl);
		// builder.append(jump + "202: Certainty Factor" + nl);
		builder.append(jump + "203: Confidence" + nl);
		// builder.append(jump + "204: Conviction" + nl);
		// builder.append(jump + "205: Leverage Rule" + nl);
		// builder.append(jump + "206: Relative Risk" + nl);
		// builder.append(jump + "207: Specificity" + nl);
		// @formatter:on
		System.out.println(builder.toString());
	}

	/**
	 * Prints the different weighting schemes
	 */
	private static void printWeightingSchemes() {
		StringBuilder builder = new StringBuilder();
		builder.append("List of weighting schemes:" + nl);
		// @formatter:off
		builder.append(jump + "1: none" + nl);
		builder.append(jump + "2: additive" + nl);
		builder.append(jump + "3: multiplicative" + nl);
		builder.append(jump + "4: adaptive" + nl);
		// @formatter:on
		System.out.println(builder.toString());
	}

	/**
	 * Parses the extra options from the command line
	 * 
	 * @param args
	 *            command line arguments in the format '-fvalue'
	 * @param options
	 *            map containing parsed options
	 * @throws InvalidOption
	 *             thrown if one of the flags is invalid (i.e. does not start with
	 *             '-'). The rest of the flags is not parsed anymore
	 */
	private static void parseOptions(String[] args, Map<String, String> options) throws InvalidOption {
		for (String arg : args) {
			if (!arg.startsWith("-")) {
				throw new InvalidOption(arg);
			}

			char option = arg.charAt(1);
			String value = arg.substring(2);

			if (option == 'q') {
				options.put(Quality, value);
			} else if (option == 'a' || option == 'p') {
				options.put(Approximation, value);
			} else if (option == 'w') {
				options.put(Weighting, value);
			} else if (option == 'o') {
				options.put(Output, value);
			}
		}
	}

	/**
	 * Gets the quality measure based on the flag value
	 * 
	 * 1=Support
	 * 
	 * @param value
	 *            flag value
	 * @param db
	 *            the transaction dataset in which the measures are computed
	 * @return the specified quality measure object
	 */
	private static ScoringFunction qualityMeasure(String value, PropositionalContext context) {
		if (value == null) {
			// return itemSetScoringFunction(logic,
			// DefaultSupportMeasurementProcedure.INSTANCE);
			return supportScoringFunction();
		}
		int id = Integer.parseInt(value);
		if (id == 1) {
			// return itemSetScoringFunction(logic,
			// DefaultSupportMeasurementProcedure.INSTANCE);
			return supportScoringFunction();
		} else if (id == 2) {
			// return itemSetScoringFunction(new AverageSubsetsSupportMeasure(db));
		}
		// return itemSetScoringFunction(logic,
		// DefaultSupportMeasurementProcedure.INSTANCE);
		return supportScoringFunction();
	}

	/**
	 * Gets the approximation measure based on the flag value
	 * 
	 * 1=Support, 2=Inverse Support, 3=Equal, 4=Average Subset Support
	 * 
	 * 101=Cosine, 102=Jaccard, 103=Kulcinsky, 104=Lift, 105=Odds Ratio 106=Relative
	 * Overlap, 107=Yule's Q, 108= Yule's Y
	 * 
	 * 201=Accuracy, 202=Certainty Factor, 203=Confidence, 204=Conviction,
	 * 205=Leverage, 206=Relative Risk, 207=Specificity
	 * 
	 * @param value
	 *            flag value
	 * @param db
	 *            the transaction dataset in which the measures are computed
	 * @return the specified approximation measure object
	 */
	private static ScoringFunction approximationMeasure(String value, PropositionalContext context) {
		if (value == null) {
			// return itemSetScoringFunction(logic,
			// DefaultSupportMeasurementProcedure.INSTANCE);
			return supportScoringFunction();
		}
		int id = Integer.parseInt(value);
		if (id == 1) {
			// return itemSetScoringFunction(logic,
			// DefaultSupportMeasurementProcedure.INSTANCE);
			return supportScoringFunction();
		} else if (id == 2) {
			// return itemSetScoringFunction(new NegativeSupportMeasure(db));
		} else if (id == 3) {
			return equalScoringFunction();
		} else if (id == 4) {
			// return itemSetScoringFunction(new AverageSubsetsSupportMeasure(db));
		} else if (id == 101) {
			// return itemSetScoringFunction(new CosineMeasure(db));
		} else if (id == 102) {
			// return itemSetScoringFunction(new JaccardMeasure(db));
		} else if (id == 103) {
			// return itemSetScoringFunction(new KulcinskyMeasure(db));
		} else if (id == 104) {
			return ruleScoringFunction(context, AssociationRuleMeasurementProcedures.RULE_LIFT);
		} else if (id == 105) {
			// return itemSetScoringFunction(new OddsRatioMeasure(db));
		} else if (id == 106) {
			// return itemSetScoringFunction(new RelativeOverlapMeasure(db));
		} else if (id == 107) {
			// return itemSetScoringFunction(new YulesQMeasure(db));
		} else if (id == 108) {
			// return itemSetScoringFunction(new YulesYMeasure(db));
		} else if (id == 201) {
			// return itemSetScoringFunction(new AccuracyMeasure(db));
		} else if (id == 202) {
			// return itemSetScoringFunction(new CertaintyFactorMeasure(db));
		} else if (id == 203) {
			return ruleScoringFunction(context, DefaultConfidenceMeasurementProcedure.INSTANCE);
		} else if (id == 204) {
			// return itemSetScoringFunction(new ConvictionMeasure(db));
		} else if (id == 205) {
			// return
			// itemSetScoringFunction(AssociationRuleMeasurementProcedures.RULE_LIFT);
		} else if (id == 206) {
			// return itemSetScoringFunction(new RelativeRiskMeasure(db));
		} else if (id == 207) {
			// return itemSetScoringFunction(new SpecificityMeasure(db));
		}
		// return itemSetScoringFunction(logic,
		// DefaultSupportMeasurementProcedure.INSTANCE);
		return supportScoringFunction();
	}

	/**
	 * Gets the weigthing scheme based on the flag value
	 * 
	 * 1=No Weighting, 2=Additive, 3=Multiplicative, 4=Adaptive
	 * 
	 * @param value
	 *            flag value
	 * @return the specified weighting scheme object
	 */
	private static WeightingScheme weightingScheme(String value) {
		if (value == null) {
			return noWeightScheme();
		}
		int id = Integer.parseInt(value);
		if (id == 1) {
			return noWeightScheme();
		} else if (id == 2) {
			return additiveWeightingScheme();
		} else if (id == 3) {
			return multiplicativeWeightingScheme();
		} else if (id == 4) {
			return adaptiveWeightingScheme();
		}
		return noWeightScheme();
	}

	/**
	 * Gets the set reporter based on the flag value. I.e. if -o has been specified,
	 * the reporter will write to file, otherwise it will write to commandline.
	 * 
	 * @param value
	 *            flag value
	 * @return a FileReporter if the value is non null, otherwise a CMDReporter
	 */
	private static SetReporter setReporter(String value) {
		if (value == null) {
			return cmdReporter();
		}
		if (value.length() != 0) {
			return fileReporter(value);
		}
		return cmdReporter();
	}
}
