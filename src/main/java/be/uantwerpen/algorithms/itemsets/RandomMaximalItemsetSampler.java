/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.algorithms.itemsets;

import static be.uantwerpen.util.ScoringFunctions.itemSetScoringFunction;
import static be.uantwerpen.util.SetReporters.nullReporter;
import static be.uantwerpen.util.Utils.logIndexSearch;
import static be.uantwerpen.util.WeightingSchemes.noWeightScheme;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.util.ScoringFunction;
import be.uantwerpen.util.SetReporter;
import be.uantwerpen.util.WeightingScheme;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.association.DefaultSupportMeasurementProcedure;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * Implements a random maximal itemset miner that can use different itemset
 * measures to compute a distribution on the possible maximal itemsets that be
 * still be generated.
 * 
 * @author Sandy Moens
 * @version 3.0.0
 * @since 2.0.0
 */
public class RandomMaximalItemsetSampler {

	public static boolean verbose = true;

	// random number generator used during random walk
	private final static Random random = new Random(System.currentTimeMillis());

	private final PropositionalContext context;

	private final List<? extends Proposition> allItems;

	private double[] arrayValues;

	// measure pruning the search space and defining the border of maximal sets
	private ScoringFunction qualityMeasure;

	// measure giving probabilities to different branches
	private ScoringFunction approximationMeasure;

	// weighting scheme used to create diversity in pattern collection
	private WeightingScheme weightingScheme;

	// reporter of found maximal itemsets
	private SetReporter setReporter;

	// minimum rank of itemsets for the quality measure
	private int minRank;

	// the minimum size of the itemset
	private int minSize;

	private LogicalDescriptor currentLogicalDescriptor;

	private List<? extends Proposition> extensions;

	private volatile boolean running;

	public RandomMaximalItemsetSampler(PropositionalContext context) {
		this.context = context;
		this.minSize = 0;
		this.allItems = context.propositions();
		this.arrayValues = new double[this.allItems.size()];
		this.setReporter = nullReporter();
		this.qualityMeasure = itemSetScoringFunction(context, DefaultSupportMeasurementProcedure.INSTANCE);
		this.approximationMeasure = itemSetScoringFunction(context, DefaultSupportMeasurementProcedure.INSTANCE);
		this.weightingScheme = noWeightScheme();
	}

	public void setReporter(SetReporter setReporter) {
		this.setReporter = setReporter;
	}

	/**
	 * Sets the measure that is used to prune the search space for maximal sets
	 * 
	 * @param measure
	 *            itemset measure that is used to prune the search space
	 */
	public void pruningMeasure(ScoringFunction measure) {
		this.qualityMeasure = measure;
	}

	/**
	 * Gets the measure that is used to prune the search space for maximal sets
	 * 
	 * @return itemset measure that is used to prune the search space
	 */
	public ScoringFunction pruningMeasure() {
		return this.qualityMeasure;
	}

	/**
	 * Sets the measure that creates a distribution over possible extensions
	 * 
	 * @param measure
	 *            itemset measure that is used to score extensions
	 */
	public void approximationMeasure(ScoringFunction measure) {
		this.approximationMeasure = measure;
	}

	/**
	 * Gets the measure that creates a distribution over possible extensions
	 * 
	 * @return itemset measure that is used to score extensions
	 */
	public ScoringFunction approximationMeasure() {
		return this.approximationMeasure;
	}

	/**
	 * Sets the weighting scheme of this sampler used to downgrade weights
	 * 
	 * @param weightingScheme
	 *            weighting scheme that is applied when computing weights
	 */
	public void weightingScheme(WeightingScheme weightingScheme) {
		this.weightingScheme = weightingScheme;
	}

	/**
	 * Gets the weighting scheme of this sampler used to downgrade weights
	 * 
	 * @return weighting scheme that is applied when computing weights
	 */
	public WeightingScheme weightingScheme() {
		return this.weightingScheme;
	}

	/**
	 * Sets the minimum size of an itemset that is to be found
	 * 
	 * @param minSize
	 *            the mimimum size of an itemset
	 */
	public void minSize(int minSize) {
		this.minSize = minSize;
	}

	public void stop() {
		this.running = false;
	}

	/**
	 * Runs the miner with specified minimal rank threshold and generates the
	 * specified number of itemsets.
	 * 
	 * @param minRank
	 *            minimal rank of the maximal itemsets
	 * @param numberOfItemSets
	 *            number of itemsets that need to be sampled
	 */
	public void run(int minRank, int numberOfItemSets) {
		this.running = true;

		this.minRank = minRank;

		List<? extends Proposition> prunedItems = pruneExtensionsByMinRank();

		if (prunedItems.isEmpty() || prunedItems.size() < this.minSize) {
			return;
		}

		int numberOfTries = 0;

		List<LogicalDescriptor> itemSets = newLinkedList();
		Set<LogicalDescriptor> sets = newHashSet();

		do {
			this.extensions = newArrayList(prunedItems);
			this.currentLogicalDescriptor = LogicalDescriptors.create(this.context.population(), ImmutableList.of());
			extendCurrentItemSet();

			if (!this.currentLogicalDescriptor.isEmpty()
					&& this.currentLogicalDescriptor.elements().size() >= this.minSize) {
				itemSets.add(this.currentLogicalDescriptor);
				sets.add(this.currentLogicalDescriptor);
				this.weightingScheme.decreaseWeight(this.currentLogicalDescriptor);
				this.setReporter.report(this.currentLogicalDescriptor);
			}
			numberOfTries++;

			if (!this.running || numberOfTries > numberOfItemSets * 10) {
				break;
			}
		} while (itemSets.size() < numberOfItemSets && numberOfTries <= numberOfItemSets * 10);
	}

	/**
	 * Prunes the list of all items based on the minimal rank for the quality
	 * measure
	 * 
	 * @return a new list of items that is pruned based on minimal rank
	 */
	private List<? extends Proposition> pruneExtensionsByMinRank() {
		List<? extends Proposition> items = newLinkedList(this.allItems);

		Iterator<? extends Proposition> it = items.iterator();
		while (it.hasNext()) {
			Proposition proposition = it.next();
			if (this.qualityMeasure.evaluate(proposition) < this.minRank) {
				it.remove();
			}
		}

		return items;
	}

	/**
	 * Prunes the list of items based on minimal rank for the quality measure
	 */
	private void pruneExtensionsForCurrentItemSet() {
		Iterator<? extends Proposition> it = this.extensions.iterator();
		while (it.hasNext()) {
			Proposition proposition = it.next();
			if (this.qualityMeasure.evaluate(this.currentLogicalDescriptor, proposition) < this.minRank) {
				it.remove();
			}
		}
	}

	/**
	 * Prunes the list of items based on minimal rank for the quality measure and
	 * add elements to construct the closed itemsets.
	 */
	private void pruneExtensionsForCurrentItemSetAndAddClosed() {
		double support = this.qualityMeasure.evaluate(this.currentLogicalDescriptor);

		List<Proposition> toAdd = Lists.newLinkedList();

		Iterator<? extends Proposition> it = this.extensions.iterator();
		while (it.hasNext()) {
			Proposition proposition = it.next();
			double eSupport = this.qualityMeasure.evaluate(this.currentLogicalDescriptor, proposition);

			if (eSupport < this.minRank) {
				it.remove();
			} else if (eSupport == support) {
				toAdd.add(proposition);
				it.remove();
			}
		}

		if (!toAdd.isEmpty()) {
			toAdd.addAll(this.currentLogicalDescriptor.elements());
			this.currentLogicalDescriptor = LogicalDescriptors.create(this.context.population(), toAdd);
		}
	}

	/**
	 * Extends the current itemset until it is maximal for the specified rank and
	 * quality measure
	 */
	private void extendCurrentItemSet() {
		pruneExtensionsForCurrentItemSet();
		while (this.extensions.size() > 0) {
			Proposition nextProposition = sampleExtension();
			this.currentLogicalDescriptor = this.currentLogicalDescriptor.specialization(nextProposition);
			this.extensions.remove(nextProposition);

			if (pruningMeasure().name().equals("Support")) {
				pruneExtensionsForCurrentItemSetAndAddClosed();
			} else {
				pruneExtensionsForCurrentItemSet();
			}
		}
	}

	/**
	 * Samples a new extension based on the probability distribution specified by
	 * the approximation measure and the weightings scheme
	 * 
	 * @return a new item from the list of extensions
	 */
	private Proposition sampleExtension() {
		if (this.extensions.size() == 1) {
			return this.extensions.get(0);
		}

		this.arrayValues = computeCumulatedValues();

		if (this.arrayValues[this.extensions.size() - 1] == 0) {
			return this.extensions.get(random.nextInt(this.extensions.size()));
		}

		int itemIx = logIndexSearch(this.arrayValues, this.extensions.size(),
				random.nextDouble() * this.arrayValues[this.extensions.size() - 1]);

		// same as nonZero check?
		if (itemIx == this.extensions.size()) {
			itemIx--;
			itemIx = random.nextInt(this.extensions.size());
		}

		return this.extensions.get(itemIx);
	}

	private double[] computeCumulatedValues() {
		int i = 0;
		double cum = 0;

		for (Proposition item : this.extensions) {
			double value = this.approximationMeasure.evaluate(this.currentLogicalDescriptor, item);
			value *= this.weightingScheme.weight(item);
			this.arrayValues[i++] = (cum += value);
		}

		return this.arrayValues;
	}
}
