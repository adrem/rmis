/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.util;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * @author Sandy Moens
 * @version 3.0.0
 * @since 3.0.0
 */
public class WeightingSchemes {

	public static WeightingScheme noWeightScheme() {
		return new NoWeightScheme();
	}

	public static WeightingScheme additiveWeightingScheme() {
		return new AdditiveWeightingScheme();
	}

	public static WeightingScheme multiplicativeWeightingScheme() {
		return new MultiplicativeWeightingScheme();
	}

	public static WeightingScheme adaptiveWeightingScheme() {
		return new AdaptiveWeightingScheme();
	}

	private static class NoWeightScheme implements WeightingScheme {

		@Override
		public String name() {
			return "No Weights";
		}

		@Override
		public void decreaseWeight(LogicalDescriptor logicalDescriptor) {
		}

		@Override
		public double weight(Proposition proposition) {
			return 1.0;
		}

		@Override
		public void reset() {
		}

	}

	private static abstract class OccurrencesBasedWeightingScheme implements WeightingScheme {
		protected Map<Proposition, AtomicInteger> propositionMap;

		public OccurrencesBasedWeightingScheme() {
			this.propositionMap = newHashMap();
		}

		public void occurrencesMap(Map<Proposition, AtomicInteger> itemMap) {
			this.propositionMap = itemMap;
		}

		public void updateOccurrence(Proposition proposition, int newOccurrence) {
			AtomicInteger count = this.propositionMap.get(proposition);
			if (count == null) {
				count = new AtomicInteger(0);
				this.propositionMap.put(proposition, count);
			}
			count.set(newOccurrence);
		}

		@Override
		public void decreaseWeight(LogicalDescriptor logicalDescriptor) {
			for (Proposition proposition : logicalDescriptor.elements()) {
				AtomicInteger count = this.propositionMap.get(proposition);
				if (count == null) {
					count = new AtomicInteger(0);
					this.propositionMap.put(proposition, count);
				}
				count.incrementAndGet();
			}
		}

		@Override
		public void reset() {
			this.propositionMap.clear();
		}

		public abstract OccurrencesBasedWeightingScheme createCopy();
	}

	private static class AdditiveWeightingScheme extends OccurrencesBasedWeightingScheme {

		@Override
		public String name() {
			return "Additive Weights";
		}

		@Override
		public double weight(Proposition proposition) {
			AtomicInteger count = this.propositionMap.get(proposition);
			if (count == null) {
				return 1;
			}
			return 1.0 / (count.get() + 1);
		}

		@Override
		public OccurrencesBasedWeightingScheme createCopy() {
			AdditiveWeightingScheme ws = new AdditiveWeightingScheme();
			for (Entry<Proposition, AtomicInteger> entry : this.propositionMap.entrySet()) {
				ws.propositionMap.put(entry.getKey(), new AtomicInteger(entry.getValue().get()));
			}
			return ws;
		}
	}

	private static class MultiplicativeWeightingScheme extends OccurrencesBasedWeightingScheme {

		private final double gamma;

		public MultiplicativeWeightingScheme() {
			this.gamma = 0.9;
		}

		@Override
		public String name() {
			return "Multiplicative Weights";
		}

		@Override
		public double weight(Proposition proposition) {
			AtomicInteger count = this.propositionMap.get(proposition);
			if (count == null) {
				return 1;
			}
			return Math.pow(this.gamma, count.get());
		}

		@Override
		public OccurrencesBasedWeightingScheme createCopy() {
			MultiplicativeWeightingScheme ws = new MultiplicativeWeightingScheme();
			for (Entry<Proposition, AtomicInteger> entry : this.propositionMap.entrySet()) {
				ws.propositionMap.put(entry.getKey(), new AtomicInteger(entry.getValue().get()));
			}
			return ws;
		}
	}

	private static class AdaptiveWeightingScheme extends OccurrencesBasedWeightingScheme {
		private double total;

		public AdaptiveWeightingScheme() {
			this.total = 0;
		}

		@Override
		public String name() {
			return "Adaptive Weights";
		}

		@Override
		public void occurrencesMap(Map<Proposition, AtomicInteger> itemMap) {
			super.occurrencesMap(itemMap);
			this.total = 0;
			for (Entry<Proposition, AtomicInteger> entry : itemMap.entrySet()) {
				this.total += entry.getValue().get();
			}
		}

		@Override
		public void updateOccurrence(Proposition proposition, int newOccurrence) {
			AtomicInteger oldOccurrence = this.propositionMap.get(proposition);
			if (oldOccurrence != null) {
				this.total += newOccurrence - oldOccurrence.get();
			}
			super.updateOccurrence(proposition, newOccurrence);
		}

		@Override
		public void decreaseWeight(LogicalDescriptor logicalDescriptor) {
			this.total += 1;
			super.decreaseWeight(logicalDescriptor);
		}

		@Override
		public double weight(Proposition proposition) {
			AtomicInteger count = this.propositionMap.get(proposition);
			if (count == null) {
				return 1;
			}
			return 1 - 1.0 * (count.get()) / this.total;
		}

		@Override
		public OccurrencesBasedWeightingScheme createCopy() {
			AdaptiveWeightingScheme ws = new AdaptiveWeightingScheme();
			for (Entry<Proposition, AtomicInteger> entry : this.propositionMap.entrySet()) {
				ws.propositionMap.put(entry.getKey(), new AtomicInteger(entry.getValue().get()));
			}
			ws.total = this.total;
			return ws;
		}
	}

	// Suppress default constructor for non-instantiability
	private WeightingSchemes() {
		throw new AssertionError();
	}

}
