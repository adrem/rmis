/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.util;

import static java.util.Arrays.fill;

/**
 * Utility functions
 * 
 * @author Sandy Moens
 * @version 3.0.0
 * @since 3.0.0
 */
public class Utils {

	public static int logIndexSearch(double[] values, double theEnd, double toSearch) {
		double left = 0;
		double right = theEnd;
		double split;

		if (right % 2 == 0) {
			while (right - left > 3) {
				split = (left + (right - left) / 2.0);
				if (values[(int) split] >= toSearch) {
					right = split;
				} else {
					left = split;
				}
			}
			int end = values.length - (int) left;
			for (int i = 0; i < end; i++) {
				if (values[(int) left + i] >= toSearch) {
					right = left + i;
					break;
				}
			}
		} else {
			while (right - left >= 1) {
				split = (left + (right - left) / 2.0);
				if (values[(int) split] >= toSearch) {
					right = split;
				} else {
					left = split;
				}
			}
		}
		// if ((right == 1 && values[0] == values[1]) || right == 1
		// && values[0] > toSearch) {
		// return 0;
		// }
		return (int) (right);
	}

	/**
	 * Gives equal sized partitions for n. If n is not dividable by p, the last
	 * partition is smaller. Useful method for partitioning for Multi-processing.
	 * 
	 * @param n
	 *            Number to partition.
	 * @param p
	 *            Number of partitions.
	 * @return An p-sized int array that has the partition sizes.
	 */
	public static int[] partition(int n, int p) {
		final int shareSize = n / p;
		final int leftOver = n - (p - 1) * shareSize;

		final int[] shareSizes = new int[p];
		fill(shareSizes, shareSize);
		shareSizes[shareSizes.length - 1] = leftOver;
		return shareSizes;
	}

}
