/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.util;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * Interface for the scoring function used by the sampler. The scoring function
 * ranks an extension of an itemset by its combination.
 * 
 * @author Sandy Moens
 * @version 3.0.0
 * @since 2.0.0
 */
public interface ScoringFunction {

	public String name();

	public double evaluate(Proposition proposition);

	public double evaluate(LogicalDescriptor logicalDescriptor);

	public double evaluate(LogicalDescriptor logicalDescriptor, Proposition proposition);

}
