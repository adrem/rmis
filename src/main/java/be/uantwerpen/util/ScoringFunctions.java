/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.util;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.rules.RuleDescriptor;

/**
 * @author Sandy Moens
 * @version 3.0.0
 * @since 3.0.0
 */
public class ScoringFunctions {

	public static ScoringFunction itemSetScoringFunction(PropositionalContext context,
			MeasurementProcedure<Measure, PatternDescriptor> m) {
		return new ItemSetScoringFunction(context, m);
	}

	public static ScoringFunction supportScoringFunction() {
		return new SupportScoringFunction();
	}

	public static ScoringFunction ruleScoringFunction(PropositionalContext context,
			MeasurementProcedure<Measure, PatternDescriptor> m) {
		return new RuleScoringFunction(context, m);
	}

	public static ScoringFunction equalScoringFunction() {
		return new EqualScoringFunction();
	}

	/**
	 * Implements an itemset based scoring function. An itemset and its extension
	 * are merged into one itemset and then evaluated.
	 * 
	 * @author Sandy Moens
	 * @version 3.0.0
	 * @since 2.0.0
	 */
	private static class ItemSetScoringFunction implements ScoringFunction {

		private final PropositionalContext context;
		private final MeasurementProcedure<Measure, PatternDescriptor> m;

		public ItemSetScoringFunction(PropositionalContext context,
				MeasurementProcedure<Measure, PatternDescriptor> m) {
			this.context = context;
			this.m = m;
		}

		@Override
		public String name() {
			return this.m.getMeasure().caption();
		}

		@Override
		public double evaluate(Proposition proposition) {
			return evaluate(LogicalDescriptors.create(this.context.population(), ImmutableList.of(proposition)));
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor) {
			return this.m.perform(logicalDescriptor).value();
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor, Proposition proposition) {
			return evaluate(logicalDescriptor.specialization(proposition));
		}

	}

	private static class SupportScoringFunction implements ScoringFunction {

		@Override
		public String name() {
			return "Support";
		}

		@Override
		public double evaluate(Proposition proposition) {
			return proposition.supportSet().size();
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor) {
			return logicalDescriptor.supportSet().size();
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor, Proposition proposition) {
			return IndexSets.intersection(logicalDescriptor.supportSet(), proposition.supportSet()).size();
		}

	}

	/**
	 * Implements a rule based scoring function. The itemset is used as left hand
	 * side, the extensions is used as right hand side of the rule.
	 * 
	 * @author Sandy Moens
	 * @version 3.0.0
	 * @since 2.0.0
	 */
	private static class RuleScoringFunction implements ScoringFunction {

		private final PropositionalContext context;

		private final MeasurementProcedure<Measure, PatternDescriptor> m;

		private final LogicalDescriptor empty;

		public RuleScoringFunction(PropositionalContext context, MeasurementProcedure<Measure, PatternDescriptor> m) {
			this.context = context;
			this.m = m;
			this.empty = LogicalDescriptors.create(this.context.population(), ImmutableList.of());
		}

		@Override
		public String name() {
			return this.m.getMeasure().caption();
		}

		@Override
		public double evaluate(Proposition proposition) {
			return evaluate(LogicalDescriptors.create(this.context.population(), ImmutableList.of(proposition)));
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor) {
			RuleDescriptor ruleDescriptor = DefaultRuleDescriptor.create(this.context, logicalDescriptor, this.empty);

			return this.m.perform(ruleDescriptor).value();
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor, Proposition proposition) {
			LogicalDescriptor consequent = LogicalDescriptors.create(this.context.population(),
					ImmutableList.of(proposition));
			RuleDescriptor ruleDescriptor = DefaultRuleDescriptor.create(this.context, logicalDescriptor, consequent);

			return this.m.perform(ruleDescriptor).value();
		}

	}

	/**
	 * Class implementing the equal ranking function, i.e. all items/itemsets are
	 * given a weight of 1
	 * 
	 * @author Sandy Moens
	 * @version 3.0.0
	 * @since 2.0.0
	 */
	private static class EqualScoringFunction implements ScoringFunction {

		@Override
		public String name() {
			return "Equal";
		}

		@Override
		public double evaluate(Proposition proposition) {
			return 1.0;
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor) {
			return 1.0;
		}

		@Override
		public double evaluate(LogicalDescriptor logicalDescriptor, Proposition proposition) {
			return 1.0;
		}

	}

	// Suppress default constructor for non-instantiability
	private ScoringFunctions() {
		throw new AssertionError();
	}

}
