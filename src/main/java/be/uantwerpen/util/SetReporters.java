/*
 * Copyright 2018 University of Antwerp
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.uantwerpen.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * @author Sandy Moens
 * @version 3.0.0
 * @since 3.0.0
 */
public class SetReporters {

	public static SetReporter nullReporter() {
		return new NullReporter();
	}

	public static SetReporter cmdReporter() {
		return new CMDReporter();
	}

	public static SetReporter fileReporter(String fileName) {
		return new FileReporter(fileName);
	}

	private static class NullReporter implements SetReporter {
		@Override
		public void report(LogicalDescriptor logicalDescriptor) {
		}

		@Override
		public void close() {
		}
	}

	private static class CMDReporter implements SetReporter {
		private static final String nl = "\n";
		private static final int BufferSize = 1;

		private final StringBuilder builder;
		private int counter;

		public CMDReporter() {
			this.builder = new StringBuilder();
			this.counter = 0;
		}

		@Override
		public void report(LogicalDescriptor logicalDescriptor) {
			for (Proposition proposition : logicalDescriptor.elements()) {
				this.builder.append(proposition.name() + " ");
			}
			this.builder.setLength(this.builder.length() - 1);
			this.builder.append(nl);

			this.counter++;
			if (this.counter % BufferSize == 0) {
				System.out.print(this.builder.toString());
				this.builder.setLength(0);
			}
		}

		@Override
		public void close() {
			if (this.builder.length() != 0) {
				System.out.print(this.builder.toString());
				this.builder.setLength(0);
			}
		}
	}

	private static class FileReporter implements SetReporter {
		private BufferedWriter writer;

		public FileReporter(String fileName) {
			try {
				this.writer = new BufferedWriter(new FileWriter(fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void report(LogicalDescriptor logicalDescriptor) {
			try {
				for (Proposition proposition : logicalDescriptor.elements()) {
					this.writer.append(proposition.name() + " ");
				}
				this.writer.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void close() {
			try {
				this.writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Suppress default constructor for non-instantiability
	private SetReporters() {
		throw new AssertionError();
	}

}
